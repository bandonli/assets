var base_host = 'http://127.0.0.1:1018/assets/';
seajs.config({
  'base':base_host + 'assets/',
  'paths': {
    'mod': base_host + 'assets/src',
    'appcss':base_host + 'assets/app/css',
    'appjs':base_host + 'assets/app/js'
  },
  'alias': {    
    'jquery':base_host + 'assets/src/jquery/jquery.js'
  }
});