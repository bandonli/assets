// 实践gruntjs
module.exports = function(grunt){

    // 载入concat和uglify插件，分别对于合并和压缩 ✔
    grunt.loadNpmTasks('grunt-cmd-transport');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');

    var transport = require('grunt-cmd-transport');

    var style = transport.style.init(grunt);
    var text = transport.text.init(grunt);
    var script = transport.script.init(grunt);

    grunt.initConfig({
        pkg : grunt.file.readJSON('package.json'),
        transport: {
            options : {
                paths : ['.'],
                alias: '<%= pkg.spm.alias %>',
                parsers : {
                    '.js' : [script.jsParser],
                    '.css' : [style.css2jsParser],
                    '.html' : [text.html2jsParser]
                },
                debug: false
            },
            domop: {
                files: [{
                    expand: true,
                    src: ['src/*'],
                    dest: 'dest/',
                    filter : 'isFile'
                }]
            }

        },
        concat : {
            domop : {
                src: ['dest/src/base.js', 'dest/src/select.js'],
                dest: 'dest/domop.js'
            }
        },
        uglify : {
            options : {
                banner : '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            },
            build : {
                src : 'dest/domop.js',
                dest : 'dest/domop.min.js'
            }
        }
    });
    // grunt.initConfig({
    //     'pkg': grunt.file.readJSON('package.json'),
    //     'transport': {}
    // });
    // 注册任务 ✔
    // grunt.registerTask('default', ['transport', 'concat', 'uglify']);
    grunt.registerTask('default', ['transport', 'concat']);
}